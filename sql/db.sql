create table rooms
(
    id            bigserial
        primary key,
    person_number bigint,
    price         bigint,
    room_class    varchar(255),
    status        varchar(255)
);

alter table rooms
    owner to postgres;

create table images
(
    id      bigserial
        primary key,
    balcony varchar(255),
    bedroom varchar(255),
    toilet  varchar(255),
    room_id bigint
        constraint fkdeh4h59nedlwji0j8e57hu9mf
            references rooms
);

alter table images
    owner to postgres;

create table users
(
    id         bigserial
        primary key,
    email      varchar(255),
    first_name varchar(255),
    last_name  varchar(255),
    password   varchar(255),
    role       varchar(255)
);

alter table users
    owner to postgres;

create table applications
(
    id         bigserial
        primary key,
    first_date date,
    last_date  date,
    room_class varchar(255),
    client_id  bigint
        constraint fkecsplrpqtt3y4css1tju3v9fe
            references users
            ON DELETE CASCADE
);

alter table applications
    owner to postgres;

create table applications_room
(
    application_id bigint not null
            references applications
            ON DELETE CASCADE,
    room_id        bigint not null
            references rooms,
    primary key (application_id, room_id)
);

alter table applications_room
    owner to postgres;

create table orders
(
    id            bigserial
        primary key,
    creation_date date,
    first_date    date,
    last_date     date,
    price         bigint,
    status        varchar(255),
    client_id     bigint
        constraint fkojjigrbyd7qrcwrxvr7e9bdr2
            references users,
    room_id       bigint
        constraint fkmvji5dgxi79luuluamunmw73h
            references rooms
);

alter table orders
    owner to postgres;

create table invoices
(
    id       bigserial
        primary key,
    status   integer,
    order_id bigint
        constraint fk4ko3y00tkkk2ya3p6wnefjj2f
            references orders
);

alter table invoices
    owner to postgres;





insert into users values (10, 'b@b.com', 'Sasha', 'Ogurov', 'a', 'ROLE_USER');
insert into users values (11, 'a@a.com', 'Sasha', 'Ogurov', 'a', 'ROLE_MANAGER');
insert into rooms values (1, 4, 100, 'ECONOMY', 'FREE');
insert into rooms values (2, 4, 100, 'ECONOMY', 'FREE');
insert into rooms values (3, 4, 200, 'STANDARD', 'FREE');
insert into rooms values (4, 4, 200, 'STANDARD', 'FREE');
insert into rooms values (5, 4, 300, 'JUNIOR_SUITE', 'FREE');
insert into rooms values (6, 4, 300, 'JUNIOR_SUITE', 'FREE');
insert into rooms values (7, 4, 400, 'SUITE', 'FREE');
insert into rooms values (8, 4, 400, 'SUITE', 'FREE');
insert into images values (1, 'room_economy_1_balcony.jpg', 'room_economy_1.jpg', 'room_economy_1_toilet.jpg', 1);
insert into images values (2, 'room_economy_2_balcony.jpg', 'room_economy_2.jpg', 'room_economy_2_toilet.jpg', 2);
insert into images values (3, 'room_standard_1_balcony.jpg', 'room_standard_1.jpg', 'room_standard_1_toilet.jpg', 3);
insert into images values (4, 'room_standard_2_balcony.jpg', 'room_standard_2.jpg', 'room_standard_2_toilet.jpg', 4);
insert into images values (5, 'room_junior_suite_1_balcony.jpg', 'room_junior_suite_1.jpg', 'room_junior_suite_1_toilet.jpg', 5);
insert into images values (6, 'room_junior_suite_2_balcony.jpg', 'room_junior_suite_2.jpg', 'room_junior_suite_2_toilet.jpg', 6);
insert into images values (7, 'room_suite_1_balcony.jpg', 'room_suite_1.jpg', 'room_suite_1_toilet.jpg', 7);
insert into images values (8, 'room_suite_2_balcony.jpg', 'room_suite_2.jpg', 'room_suite_2_toilet.jpg', 8);
insert into orders (id, creation_date, first_date, last_date, price, status, client_id, room_id) values (9, '2011-12-12', '2022-12-12', '2022-12-12', 300, 'NOT_PAID', 10, 1);
insert into orders (id, creation_date, first_date, last_date, price, status, client_id, room_id) values (10, '2020-12-12', '2022-11-12', '2022-12-12', 600, 'NOT_PAID', 10, 1);
insert into orders (id, creation_date, first_date, last_date, price, status, client_id, room_id) values (11, '2012-12-12', '2022-09-12', '2022-10-12', 400, 'NOT_PAID', 10, 2);
insert into orders (id, creation_date, first_date, last_date, price, status, client_id, room_id) values (12, '2012-01-01', '2022-01-02', '2022-01-01', 400, 'PAID', 10, 2);

select count(o.id) from rooms as r join orders as o on r.id = o.room_id where r.id = 1
                                                                          and (o.first_date <= '2011-12-12' and o.last_date >= '2011-12-12') GROUP BY r.id;

alter table applications
    drop constraint FKecsplrpqtt3y4css1tju3v9fe;
alter table applications_room
    drop constraint FKm9s4udc6ti5y2nnjhiwb0e6s3;
alter table applications_room
    drop constraint FKbpc06pugkv94nc37ostqcr8i;
alter table images
    drop constraint FKdeh4h59nedlwji0j8e57hu9mf;
alter table invoices
    drop constraint FK4ko3y00tkkk2ya3p6wnefjj2f;
alter table orders
    drop constraint FKojjigrbyd7qrcwrxvr7e9bdr2;
alter table orders
    drop constraint FKmvji5dgxi79luuluamunmw73h;
drop table if exists applications cascade;
drop table if exists applications_room cascade;
drop table if exists images cascade;
drop table if exists invoices cascade;
drop table if exists orders cascade;
drop table if exists rooms cascade;
drop table if exists users cascade;