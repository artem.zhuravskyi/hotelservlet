package artem.hotelproject.hotelservlet.service;

import artem.hotelproject.hotelservlet.dao.OrderDAO;
import artem.hotelproject.hotelservlet.dao.RoomDAO;
import artem.hotelproject.hotelservlet.model.Order;
import artem.hotelproject.hotelservlet.DTO.ReservationDTO;
import artem.hotelproject.hotelservlet.model.Invoice;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import static artem.hotelproject.hotelservlet.model.enums.RoomClass.ECONOMY;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceTest {

    @Spy
    @InjectMocks
    private OrderService orderService;

    @Mock
    private OrderDAO orderDAO;
    @Mock(answer = Answers.RETURNS_DEEP_STUBS)
    private RoomDAO roomDAO;
    @Mock
    private ReservationDTO reservationDTO;
    @Mock
    private InvoiceService invoiceService;
    @Mock
    private Order order;
    @Mock
    private User user;
    @Mock
    private Invoice invoice;
    @Mock
    private Room room;

    private static final Long ID = 1L;
    private static final RoomClass ROOM_CLASS = ECONOMY;
    private static final String DATE_RANGE = "01-01-2022 - 01-02-2022";
    private static final String[] DATES = {"01/01/2022", "01/02/2022"};
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");
    private Optional<Room> roomOptional;
    private static final int pageNum = 1;
    private static final int pageSize = 1;
    private static final String sorting = "price";

    @Before
    public void setUp() throws Exception {
        when(reservationDTO.getFirstDate()).thenReturn(LocalDate.parse(DATES[0], formatter));
        when(reservationDTO.getLastDate()).thenReturn(LocalDate.parse(DATES[1], formatter));
        when(reservationDTO.getRoomId()).thenReturn(ID);
        roomOptional = Optional.of(room);
        when(roomDAO.findById(ID)).thenReturn(roomOptional);
        when(room.getRoomClass()).thenReturn(ROOM_CLASS);

    }

    @Test
    public void itShouldPayOrder() {

        when(order.getInvoice()).thenReturn(invoice);
        when(orderDAO.findByClientAndId(user, ID)).thenReturn(order);
        doNothing().when(orderService).generateReport(any());

        orderService.payOrder(ID);

        verify(orderDAO).findByClientAndId(user, ID);
        verify(invoiceService).createInvoice(order);
        verify(order).getInvoice();
        verify(order).setStatus(Order.Status.PAID);
        verify(orderService).generateReport(order);

    }

    @Test
    public void itShouldFindOrdersByClient() {
        List<Order> expected = List.of(order);
        when(orderDAO.findAllByClient(user)).thenReturn(expected);

        List<Order> actual = orderService.findOrdersByClient(user);

        assertEquals(expected, actual);
    }

    @Test
    public void itShouldOrderRoom() {
        when(roomDAO.countIntersectionDateQuantity(reservationDTO.getRoomId(),
                reservationDTO.getLastDate(), reservationDTO.getFirstDate())).thenReturn(Optional.of(1));
        orderService.orderRoom(reservationDTO, user);

        verify(orderService).createOrder(room, user, reservationDTO);
    }

    @Test
    public void itShouldNotOrderRoom() {
        doReturn(Optional.empty()).when(roomDAO).countIntersectionDateQuantity(reservationDTO.getRoomId(),
                reservationDTO.getLastDate(), reservationDTO.getFirstDate());
        orderService.orderRoom(reservationDTO, user);
        verify(orderService, never()).createOrder(room, user, reservationDTO);
    }

    @Test
    public void itShouldOrderRoomById() {
        when(orderService.splitDateToFirstDateAndLastDate(DATE_RANGE)).thenReturn(DATES);
        doReturn(reservationDTO).when(orderService).createReservationDTO(DATES, ID);

        orderService.orderRoomById(DATE_RANGE, ID, user);

        verify(orderService).orderRoom(reservationDTO, user);

    }

    @Test
    public void itShouldShowAllOrdersByRoomAndLastDayAfter() {

        List<Order> expected = List.of(order);
        doReturn(expected).when(orderDAO).findAllByRoomAndLastDateAfter(room, LocalDate.now());

        List<Order> actual = orderService.showAllOrdersByRoomAndLastDayAfter(ID);

        assertEquals(expected, actual);
    }

}
