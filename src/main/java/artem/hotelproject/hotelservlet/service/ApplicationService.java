package artem.hotelproject.hotelservlet.service;

import artem.hotelproject.hotelservlet.DTO.ReservationDTO;
import artem.hotelproject.hotelservlet.dao.ApplicationDAO;
import artem.hotelproject.hotelservlet.dao.RoomDAO;
import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.Application;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static artem.hotelproject.hotelservlet.model.Room.Status.BOOKED;


public class ApplicationService {

    private final ApplicationDAO applicationDAO;
    private final RoomDAO roomDAO;
    private final OrderService orderService;
    private static ApplicationService instance;

    public static synchronized ApplicationService getInstance() throws SQLException {
        if (instance == null) {
            instance = new ApplicationService(OrderService.getInstance(), ApplicationDAO.getInstance(), RoomDAO.getInstance());
        }
        return instance;
    }

    private ApplicationService(OrderService orderService, ApplicationDAO applicationDAO, RoomDAO roomDAO) {
        this.applicationDAO = applicationDAO;
        this.roomDAO = roomDAO;
        this.orderService = orderService;
    }

    public void createApplication(ReservationDTO reservationDTO, User currentUser) throws UserException {

        Application application = new Application();
        application.setClient(currentUser);
        application.setFirstDate(reservationDTO.getFirstDate());
        application.setLastDate(reservationDTO.getLastDate());
        application.setRoomClass(reservationDTO.getRoomClass());
        application.setRoom(findApplicationRoomsByFilter(reservationDTO));
        if (isApplicationExist(application)) {
            throw new UserException("Application is already exist");
        }
        applicationDAO.create(application);

    }

    private boolean isApplicationExist(Application application) {
        return applicationDAO.isApplicationExist(application);
    }


    public ReservationDTO createReservationDTO(String date, RoomClass roomClass) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");

        String[] firstDateAndLastDate = orderService.splitDateToFirstDateAndLastDate(date);

        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setFirstDate(LocalDate.parse(firstDateAndLastDate[0], formatter));
        reservationDTO.setLastDate(LocalDate.parse(firstDateAndLastDate[1], formatter));
        reservationDTO.setRoomClass(roomClass);
        return reservationDTO;
    }

    public List<Application> showClientApplications(User currentUser) {
        return applicationDAO.findAllByClient(currentUser);
    }

    public List<Application> showAllApplications() {
        return applicationDAO.findAll();
    }


    public void updateApplication(Long applicationId) {
        Application currentApp = applicationDAO.findById(applicationId).get();
        List<Room> rooms = currentApp.getRoom();
        rooms.forEach(room -> room.setStatus(BOOKED));
        rooms.forEach(roomDAO::update);
    }


    public ReservationDTO createReservationDTOFromApplication(Long roomId, Long applicationId) {
        updateApplication(applicationId);
        Room room = roomDAO.findById(roomId).get();
        Application currentApp = applicationDAO.findById(applicationId).get();
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setFirstDate(currentApp.getFirstDate());
        reservationDTO.setLastDate(currentApp.getLastDate());
        reservationDTO.setRoomClass(currentApp.getRoomClass());
        reservationDTO.setRoomId(room.getId());
        reservationDTO.setClient(currentApp.getClient());
        applicationDAO.deleteById(currentApp.getId());
        return reservationDTO;
    }

    private List<Room> findApplicationRoomsByFilter(ReservationDTO reservationDTO) {
        List<Room> rooms = roomDAO.findAll();
        return rooms.stream()
                .filter(room -> {
            if (room.getRoomClass().equals(reservationDTO.getRoomClass())) {
                reservationDTO.setRoomId(room.getId());
                return true;
            } else {
                return false;
            }
        }).collect(Collectors.toList());

    }

}
