package artem.hotelproject.hotelservlet.service;

import artem.hotelproject.hotelservlet.DTO.UserDTO;
import artem.hotelproject.hotelservlet.dao.ApplicationDAO;
import artem.hotelproject.hotelservlet.dao.RoomDAO;
import artem.hotelproject.hotelservlet.dao.UserDAO;
import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.Order;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.Role;

import java.sql.SQLException;
import java.util.List;
import java.util.NoSuchElementException;

import static artem.hotelproject.hotelservlet.messages.Messages.USER_ALREADY_EXISTS_MSG;


public class UserService {

    private final UserDAO userDAO;
    private static UserService instance;

    public static synchronized UserService getInstance() throws SQLException {
        if (instance == null) {
            instance = new UserService(UserDAO.getInstance());
        }
        return instance;
    }

    private UserService(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public void register(UserDTO userDTO, Role role) throws UserException {

        if (userDAO.findUserByEmail(userDTO.getEmail())
                .isPresent()) {
            throw new UserException(
                    String.format(USER_ALREADY_EXISTS_MSG, userDTO.getEmail())
            );
        }

        User newUser = new User();
        newUser.setFirstName(userDTO.getFirstName());
        newUser.setLastName(userDTO.getLastName());
        newUser.setPassword(userDTO.getPassword());
        newUser.setEmail(userDTO.getEmail());
                newUser.setRole(role);

        userDAO.create(newUser);
    }

    public void register(UserDTO userDTO) throws UserException{
        register(userDTO, Role.ROLE_USER);
    }

    public List<Order> getOrders(Long id) {
        return userDAO.findById(id).get().getOrders();
    }

    public User findUserByEmail(String email) throws UserException {
        return userDAO.findUserByEmail(email).orElseThrow(() -> new UserException("User does not exist"));
    }
//    @ExceptionHandler(NoSuchElementException.class)
//    @ResponseStatus(HttpStatus.NOT_FOUND)
//    protected ResponseEntity<String> handleItemNotFoundException(NoSuchElementException e) {
//        return new ResponseEntity<>("No Such Element Exceptions happen! Incorrect Data. ", HttpStatus.NOT_FOUND);
//    }
}
