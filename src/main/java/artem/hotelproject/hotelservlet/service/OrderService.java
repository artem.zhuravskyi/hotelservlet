package artem.hotelproject.hotelservlet.service;

import artem.hotelproject.hotelservlet.DTO.ReservationDTO;
import artem.hotelproject.hotelservlet.dao.ApplicationDAO;
import artem.hotelproject.hotelservlet.dao.OrderDAO;
import artem.hotelproject.hotelservlet.dao.RoomDAO;
import artem.hotelproject.hotelservlet.model.Invoice;
import artem.hotelproject.hotelservlet.model.Order;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.log4j.Logger;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

import static artem.hotelproject.hotelservlet.model.Order.Status.NOT_PAID;
import static artem.hotelproject.hotelservlet.model.Order.Status.PAID;

public class OrderService {

    private static OrderService instance;

    private final OrderDAO orderDAO;
    private final RoomDAO roomDAO;
    private final InvoiceService invoiceService;

    private static final Logger logger = Logger.getLogger(OrderService.class);
    private static final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/dd/yyyy");


    public static synchronized OrderService getInstance() {
        if (instance == null) {
            instance = new OrderService(OrderDAO.getInstance(), RoomDAO.getInstance(), InvoiceService.getInstance());
        }
        return instance;
    }


    private OrderService(OrderDAO orderDAO, RoomDAO roomDAO, InvoiceService invoiceService) {
        this.orderDAO = orderDAO;
        this.roomDAO = roomDAO;
        this.invoiceService = invoiceService;
    }

    public List<Order> findOrdersByClient(User client) {
        return orderDAO.findAllByClient(client);
    }

    private byte[] bytePdfFile(Order order) {
        try {

            List<Order> allOrder = new ArrayList<>();
            allOrder.add(orderDAO.findById(order.getId()).get());

            JasperReport jasperReport = JasperCompileManager
                    .compileReport("C:\\Users\\iisus\\Desktop\\demo\\src\\main\\resources\\templates\\jrxml\\invoice.jrxml");
            JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(allOrder);

            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, null,
                    dataSource);
            return JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (JRException ignored) {
            System.out.println("error");
        }
        return null;
    }

    public boolean isReservationDateValid(ReservationDTO reservationDTO) {
        Optional<Integer> counted = roomDAO.countIntersectionDateQuantity(reservationDTO.getRoomId(),
                reservationDTO.getLastDate(), reservationDTO.getFirstDate());
        return counted.get() == 0;
    }

    public String[] splitDateToFirstDateAndLastDate(String date) {
        return date.split(" - ");
    }

    protected ReservationDTO createReservationDTO(String[] dates, Long id) {
        ReservationDTO reservationDTO = new ReservationDTO();
        reservationDTO.setFirstDate(LocalDate.parse(dates[0], formatter));
        reservationDTO.setLastDate(LocalDate.parse(dates[1], formatter));
        reservationDTO.setRoomId(id);
        reservationDTO.setRoomClass(roomDAO.findById(id).get().getRoomClass());
        return reservationDTO;
    }

    public void orderRoomById(String dateRange, Long id, User currentUser) {

        String[] dates = splitDateToFirstDateAndLastDate(dateRange);


        orderRoom(createReservationDTO(dates, id), currentUser);
    }

    public void orderRoom(ReservationDTO reservationDTO, User currentUser) {

        if (!isReservationDateValid(reservationDTO)) {
            return;
        }

        Room room = roomDAO.findById(reservationDTO.getRoomId()).get();

        createOrder(room, currentUser, reservationDTO);
    }

    protected void generateReport(Order order) {

        final String username = "hotelCaliforniaKyiv@gmail.com";
        final String password = "bpmbmgbemyypqrln";
        String recipientEmail = "artem.zhuravskyi22@gmail.com";

        Properties prop = new Properties();
        prop.put("mail.smtp.host", "smtp.gmail.com");
        prop.put("mail.smtp.port", "587");
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.ssl.protocols", "TLSv1.2");//TLS

        Session session = Session.getInstance(prop,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {
            //construct the pdf body part
            DataSource dataSource = new ByteArrayDataSource(bytePdfFile(order), "application/pdf");
            MimeBodyPart pdfBodyPart = new MimeBodyPart();
            pdfBodyPart.setDataHandler(new DataHandler(dataSource));

            //construct the mime multi part
            MimeMultipart mimeMultipart = new MimeMultipart();
            mimeMultipart.addBodyPart(pdfBodyPart);
            pdfBodyPart.setFileName("invoice.pdf");

            //create the sender/recipient addresses
            InternetAddress iaSender = new InternetAddress(username);
            InternetAddress iaRecipient = new InternetAddress(recipientEmail);

            //construct the mime message
            MimeMessage mimeMessage = new MimeMessage(session);
            mimeMessage.setSender(iaSender);
            mimeMessage.setRecipient(Message.RecipientType.TO, iaRecipient);
            mimeMessage.setContent(mimeMultipart);

            //send off the email
            Transport.send(mimeMessage);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void payOrder(Long id) {

        Order order = orderDAO.findById(id).get();
        invoiceService.createInvoice(order);
        order.setStatus(PAID);
        generateReport(order);
        orderDAO.update(order);
    }

    public void withDrawPayment(Order order) {
        orderDAO.deleteById(order.getId());
    }
//
//    @Transactional
//    @Scheduled(fixedDelay = 100000)
//    public void scheduleFixedRateTask() {
//        System.out.println("Hello");
//        List<Order> order = orderRepository.findAllByStatus(NOT_PAID);
//        order.stream().filter(this::isDateExpired).forEach(this::withDrawPayment);
//    }

    private boolean isDateExpired(Order order) {
        return LocalDate.now().isAfter(order.getCreationDate().plusDays(2));
    }

    private Long setOrderPrice(Room room, ReservationDTO reservationDTO) {

        long lengthOfStay = countLengthOfStay(reservationDTO);

        return switch (room.getRoomClass()) {
            case ECONOMY -> 100L * lengthOfStay;
            case STANDARD -> 200L * lengthOfStay;
            case JUNIOR_SUITE -> 300L * lengthOfStay;
            case SUITE -> 400L * lengthOfStay;
        };
    }

    private Long countLengthOfStay(ReservationDTO reservationDTO) {
        return ChronoUnit.DAYS.between(reservationDTO.getFirstDate(), reservationDTO.getLastDate()) + 1L;
    }

    public void createOrder(Room room, User currentUser, ReservationDTO reservationDTO) {
        Order order = new Order();
        order.setRoom(room);
        order.setClient(currentUser);
        order.setFirstDate(reservationDTO.getFirstDate());
        order.setLastDate(reservationDTO.getLastDate());
        order.setCreationDate(LocalDate.now());
        order.setPrice(setOrderPrice(room, reservationDTO));
        order.setStatus(NOT_PAID);
        orderDAO.create(order);
    }

    public List<Long> ordersToDates(List<Order> orders) {
        ZoneId zoneId = ZoneId.systemDefault();
        logger.info("Orders: " + orders);
        return orders.stream()
                .map(x -> fromFDtoLD(x.getFirstDate(), x.getLastDate()))
                .flatMap(Collection::stream)
                .map(y -> y.atStartOfDay(zoneId).toInstant().toEpochMilli()).collect(Collectors.toList());
    }

    public List<Order> showAllOrdersByRoomAndLastDayAfter(Long id) {
        Room room = roomDAO.findById(id).get();
        return orderDAO.findAllByRoomAndLastDateAfter(room, LocalDate.now());
    }


    private List<LocalDate> fromFDtoLD(LocalDate firstDate, LocalDate lastDate) {
        List<LocalDate> dateList = new ArrayList<>();
        LocalDate temp = firstDate;
        dateList.add(temp);
        while (!temp.equals(lastDate)) {
            temp = temp.plusDays(1);
            dateList.add(temp);
        }
        return dateList;
    }
//
//    public List<Order> findOrdersWithPaginationAndSort(User client, int pageNum, int pageSize, String sorting) {
//        return orderDAO.findAllByClient(client, PageRequest.of(pageNum, pageSize).withSort(Sort.by(sorting)));
//    }

}
