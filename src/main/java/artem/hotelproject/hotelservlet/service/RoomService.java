package artem.hotelproject.hotelservlet.service;


import artem.hotelproject.hotelservlet.dao.RoomDAO;
import artem.hotelproject.hotelservlet.model.Room;

import java.sql.SQLException;
import java.util.List;

public class RoomService {

    private final RoomDAO roomDAO;
    private static RoomService instance;

    public static synchronized RoomService getInstance() {
        if (instance == null) {
            instance = new RoomService(RoomDAO.getInstance());
        }
        return instance;
    }

    private RoomService(RoomDAO roomDAO) {
        this.roomDAO = roomDAO;
    }

    public Room showRoom(Long id) {
        return roomDAO.findById(id).get();
    }

    public List<Room> showAllRooms() {
        return roomDAO.findAll();
    }


}
