package artem.hotelproject.hotelservlet.service;

import artem.hotelproject.hotelservlet.dao.InvoiceDAO;
import artem.hotelproject.hotelservlet.dao.OrderDAO;
import artem.hotelproject.hotelservlet.model.Invoice;
import artem.hotelproject.hotelservlet.model.Order;

public class InvoiceService {

    private final InvoiceDAO invoiceDAO;
    private static InvoiceService instance;

    public static synchronized InvoiceService getInstance() {
        if (instance == null) {
            instance = new InvoiceService(InvoiceDAO.getInstance());
        }
        return instance;
    }

    private InvoiceService(InvoiceDAO invoiceDAO) {
        this.invoiceDAO = invoiceDAO;
    }

    public void createInvoice(Order order) {
        OrderDAO orderDAO = OrderDAO.getInstance();
        InvoiceDAO invoiceDAO = InvoiceDAO.getInstance();
        Invoice invoice = new Invoice();
        invoice.setOrder(order);
        order.setInvoice(invoice);
        invoiceDAO.create(invoice);
        orderDAO.update(order);
    }


}
