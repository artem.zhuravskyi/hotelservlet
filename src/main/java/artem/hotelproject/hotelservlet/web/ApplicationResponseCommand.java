package artem.hotelproject.hotelservlet.web;


import artem.hotelproject.hotelservlet.DTO.ReservationDTO;
import artem.hotelproject.hotelservlet.service.ApplicationService;
import artem.hotelproject.hotelservlet.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_ALL_APPLICATIONS;
import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_LOGIN;

public class ApplicationResponseCommand extends Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (isManager(request)) {

            OrderService orderService = OrderService.getInstance();
            ApplicationService applicationService = ApplicationService.getInstance();
            Long roomId = Long.parseLong(request.getParameter("roomId"));
            Long applicationId = Long.parseLong(request.getParameter("applicationId"));
            ReservationDTO reservationDTO = applicationService.createReservationDTOFromApplication(roomId, applicationId);
            orderService.orderRoom(reservationDTO, reservationDTO.getClient());
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return REDIRECT_PAGE_ALL_APPLICATIONS;

    }

}
