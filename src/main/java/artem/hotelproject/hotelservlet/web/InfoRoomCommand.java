package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.service.RoomService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_ROOM_INFO;

public class InfoRoomCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {

        Long id = Long.parseLong(request.getRequestURI().split("/")[2]);
        RoomService roomService = RoomService.getInstance();
        request.setAttribute("room", roomService.showRoom(id));
        return PAGE_ROOM_INFO;

    }
}
