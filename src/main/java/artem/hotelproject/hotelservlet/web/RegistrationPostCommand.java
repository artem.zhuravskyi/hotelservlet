package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.DTO.UserDTO;
import artem.hotelproject.hotelservlet.DTO.Validator;
import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.Role;
import artem.hotelproject.hotelservlet.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static artem.hotelproject.hotelservlet.controller.Path.*;

public class RegistrationPostCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String email = request.getParameter("email");
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String password = request.getParameter("password");
        UserService service = UserService.getInstance();
        UserDTO userDTO = new UserDTO();
        userDTO.setEmail(email);
        userDTO.setFirstName(firstName);
        userDTO.setLastName(lastName);
        userDTO.setPassword(password);
        List<String> validationList = Validator.validate(userDTO);

        if (validationList.size() != 0) {
            request.setAttribute("validationErrors", validationList);
            return PAGE_REGISTRATION;
        }

        try {
            service.register(userDTO);
        } catch (Exception ex) {
            request.setAttribute("emailIsTaken", true);
            return PAGE_REGISTRATION;
        }
        request.setAttribute("userSuccessfullyCreated", true);
        return REDIRECT_PAGE_LOGIN;

    }
}
