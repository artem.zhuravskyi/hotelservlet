package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static artem.hotelproject.hotelservlet.controller.Path.*;


public class LoginPostCommand extends Command {

    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        UserService userService = UserService.getInstance();
        User user;

        try {
            user = userService.findUserByEmail(email);
        } catch (Exception e) {
            request.setAttribute("emailException", true);
            return REDIRECT_PAGE_LOGIN + "?emailException";
        }

        if (!password.equals(user.getPassword())) {
            return REDIRECT_PAGE_LOGIN + "?passException";
        }

        String userRole = user.getRole().toString();
        session.setAttribute("user", user);
        session.setAttribute("userRole", userRole);
        return REDIRECT_PAGE_ALL_ROOMS;
    }
}