package artem.hotelproject.hotelservlet.web;


import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;
import artem.hotelproject.hotelservlet.service.ApplicationService;
import artem.hotelproject.hotelservlet.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.*;


public class OrderRoomPostCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (isUser(request)) {
            OrderService orderService = OrderService.getInstance();
            String date = request.getParameter("dateRange");
            Long id = Long.parseLong(request.getRequestURI().split("/")[2]);
            User user = (User) request.getSession().getAttribute("user");
            orderService.orderRoomById(date, id, user);
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return REDIRECT_PAGE_ORDERS;

    }
}
