package artem.hotelproject.hotelservlet.web;


import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.OrderService;
import artem.hotelproject.hotelservlet.service.RoomService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_ROOM_ORDER;


public class OrderRoomCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        if (isUser(request)) {
            Long id = Long.parseLong(request.getRequestURI().split("/")[2]);
            OrderService orderService = OrderService.getInstance();
            RoomService roomService = RoomService.getInstance();

            request.setAttribute("reservedDates", orderService.ordersToDates(orderService.showAllOrdersByRoomAndLastDayAfter(id)));
            request.setAttribute("room", roomService.showRoom(id));

        } else {
            request.setAttribute("accessDenied", true);

        }
        return PAGE_ROOM_ORDER;
    }
}
