package artem.hotelproject.hotelservlet.web;


import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.ApplicationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_CLIENT_APPLICATIONS;
import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_LOGIN;


public class ClientApplicationsCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        if (isUser(request)) {

            User user = (User) request.getSession().getAttribute("user");
            ApplicationService applicationService = ApplicationService.getInstance();
            request.setAttribute("clientApplications", applicationService.showClientApplications(user));
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return PAGE_CLIENT_APPLICATIONS;
    }
}