package artem.hotelproject.hotelservlet.web;


import artem.hotelproject.hotelservlet.service.ApplicationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_ALL_USERS_APPLICATIONS;
import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_LOGIN;


public class AllApplicationsCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        if (isManager(request)) {
            ApplicationService applicationService = ApplicationService.getInstance();
            request.setAttribute("allApplications", applicationService.showAllApplications());
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return PAGE_ALL_USERS_APPLICATIONS;
    }
}