package artem.hotelproject.hotelservlet.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_LOGIN;

public class LogoutCommand extends Command{
    @Override
    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        session.setAttribute("user", null);
        session.setAttribute("userRole", null);
        return REDIRECT_PAGE_LOGIN;
    }
}
