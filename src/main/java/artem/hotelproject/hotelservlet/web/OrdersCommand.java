package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_ORDERS;
import static artem.hotelproject.hotelservlet.controller.Path.REDIRECT_PAGE_LOGIN;


public class OrdersCommand extends Command {
    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        if (isUser(request)) {

            User user = (User) request.getSession().getAttribute("user");
            OrderService orderService = OrderService.getInstance();
            request.setAttribute("orders", orderService.findOrdersByClient(user));
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return PAGE_ORDERS;
    }

}