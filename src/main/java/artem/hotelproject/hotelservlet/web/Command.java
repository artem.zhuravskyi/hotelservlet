package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.Role;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public abstract class Command {

    /**
     * Execution method for command.
     *
     * @return Address to go once the command is executed.
     */
    public abstract String execute(HttpServletRequest request,
                                   HttpServletResponse response) throws Exception;

    @Override
    public final String toString() {
        return getClass().getSimpleName();
    }

    public static boolean isManager(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute("user");
        Role userRole = currentUser.getRole();
        return userRole.equals(Role.ROLE_MANAGER);
    }

    public static boolean isUser(HttpServletRequest request) {
        HttpSession session = request.getSession();
        User currentUser = (User) session.getAttribute("user");
        if (currentUser == null) {
            return false;
        }
        Role userRole = currentUser.getRole();
        return userRole.equals(Role.ROLE_USER);
    }
}