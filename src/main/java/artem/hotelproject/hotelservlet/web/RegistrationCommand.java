package artem.hotelproject.hotelservlet.web;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_REGISTRATION;

public class RegistrationCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        return PAGE_REGISTRATION;
    }
}
