package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.service.RoomService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import static artem.hotelproject.hotelservlet.controller.Path.PAGE_ALL_ROOMS;

public class AllRoomsCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException, SQLException {
        RoomService roomService = RoomService.getInstance();
        List<Room> rooms = roomService.showAllRooms();
        request.setAttribute("rooms", rooms);
        return PAGE_ALL_ROOMS;
    }
}
