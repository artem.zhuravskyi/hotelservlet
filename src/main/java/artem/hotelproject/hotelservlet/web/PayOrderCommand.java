package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

import static artem.hotelproject.hotelservlet.controller.Path.*;

public class PayOrderCommand extends Command {


    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {

        if (isUser(request)) {
            Long id = Long.parseLong(request.getRequestURI().split("/")[2]);
            OrderService orderService = OrderService.getInstance();
            orderService.payOrder(id);
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }

        return REDIRECT_PAGE_ORDERS;

    }
}
