package artem.hotelproject.hotelservlet.web;


import java.util.HashMap;
import java.util.Map;

public class CommandContainer {


    private static final Map<String, Command> commands = new HashMap<>();
    static {
        commands.put("login", new LoginCommand());
        commands.put("login-post", new LoginPostCommand());
        commands.put("logout", new LogoutCommand());
        commands.put("all-rooms", new AllRoomsCommand());
        commands.put("info-room", new InfoRoomCommand());
        commands.put("pay-order", new PayOrderCommand());
        commands.put("orders", new OrdersCommand());
        commands.put("order-room-post", new OrderRoomPostCommand());
        commands.put("order-room", new OrderRoomCommand());
        commands.put("all-applications", new AllApplicationsCommand());
        commands.put("application-response", new ApplicationResponseCommand());
        commands.put("create-application", new CreateApplicationCommand());
        commands.put("create-application-post", new CreateApplicationPostCommand());
        commands.put("client-applications", new ClientApplicationsCommand());
        commands.put("registration", new RegistrationCommand());
        commands.put("registration-post", new RegistrationPostCommand());
    }

    public static Command get(String commandName) {
        if (commandName == null || !commands.containsKey(commandName)) {
            return commands.get("noCommand");
        }

        return commands.get(commandName);
    }
}
