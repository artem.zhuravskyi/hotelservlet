package artem.hotelproject.hotelservlet.web;

import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;
import artem.hotelproject.hotelservlet.service.ApplicationService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Locale;

import static artem.hotelproject.hotelservlet.controller.Path.*;

public class CreateApplicationPostCommand extends Command {

    public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
        if (isUser(request)) {
            ApplicationService applicationService = ApplicationService.getInstance();
            String date = request.getParameter("date");
            RoomClass roomClass = RoomClass.valueOf(request.getParameter("roomClass").toUpperCase(Locale.ROOT));
            User user = (User) request.getSession().getAttribute("user");
            try {
                applicationService.createApplication(applicationService.createReservationDTO(date, roomClass), user);
            } catch (UserException e) {
                return REDIRECT_PAGE_CLIENT_APPLICATIONS + "?exist";
            }
        } else {
            request.setAttribute("accessDenied", true);
            return REDIRECT_PAGE_LOGIN;
        }
        return REDIRECT_PAGE_CLIENT_APPLICATIONS;
    }
}
