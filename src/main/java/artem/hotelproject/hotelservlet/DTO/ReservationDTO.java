package artem.hotelproject.hotelservlet.DTO;

import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;

import java.time.LocalDate;

public class ReservationDTO {

    private LocalDate firstDate;
    private LocalDate lastDate;
    private RoomClass roomClass;
    private User client;
    private Long roomId;

    public LocalDate getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(LocalDate firstDate) {
        this.firstDate = firstDate;
    }

    public LocalDate getLastDate() {
        return lastDate;
    }

    public void setLastDate(LocalDate lastDate) {
        this.lastDate = lastDate;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(RoomClass roomClass) {
        this.roomClass = roomClass;
    }

    public User getClient() {
        return client;
    }

    public void setClient(User client) {
        this.client = client;
    }

    public Long getRoomId() {
        return roomId;
    }

    public void setRoomId(Long roomId) {
        this.roomId = roomId;
    }


}
