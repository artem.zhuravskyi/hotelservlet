package artem.hotelproject.hotelservlet.DTO;

import java.util.ArrayList;
import java.util.List;

public class Validator {

    public static Validator instance;
    private static final String EMAIL_REGEX = "[A-z0-9._%+-]+@[A-z0-9.-]+\\.[A-z]{2,6}";

    public static synchronized Validator getInstance() {
        if (instance == null) {
            instance = new Validator();
        }
        return instance;
    }


    public static List<String> validate(Object o) {
        if (o.getClass() == UserDTO.class) {
            return userValidation((UserDTO) o);
        }

        return new ArrayList<>();
    }

    private static List<String> userValidation(UserDTO userDTO) {
        List<String> list = new ArrayList<>();

        if (userDTO.getFirstName() == null ||
                userDTO.getFirstName().length() < 3 ||
                userDTO.getFirstName().length() > 30) {
            list.add("firstName");
        }

        if (userDTO.getLastName() == null ||
                userDTO.getLastName().length() < 3 ||
                userDTO.getLastName().length() > 30) {
            list.add("lastName");
        }

        if (userDTO.getEmail() == null ||
                !userDTO.getEmail().matches(EMAIL_REGEX)) {
            list.add("email");
        }

        if (userDTO.getPassword() == null ||
                userDTO.getPassword().length() < 3 ||
                userDTO.getPassword().length() > 30) {
            list.add("password");
        }

        return list;
    }

}
