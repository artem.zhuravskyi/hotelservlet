package artem.hotelproject.hotelservlet.DTO;

import artem.hotelproject.hotelservlet.model.enums.RoomClass;


import java.time.LocalDate;

public class ApplicationDTO {

    private LocalDate firstDate;
    private LocalDate lastDate;
    private RoomClass roomClass;

    public LocalDate getFirstDate() {
        return firstDate;
    }

    public void setFirstDate(LocalDate firstDate) {
        this.firstDate = firstDate;
    }

    public LocalDate getLastDate() {
        return lastDate;
    }

    public void setLastDate(LocalDate lastDate) {
        this.lastDate = lastDate;
    }

    public RoomClass getRoomClass() {
        return roomClass;
    }

    public void setRoomClass(RoomClass roomClass) {
        this.roomClass = roomClass;
    }
}
