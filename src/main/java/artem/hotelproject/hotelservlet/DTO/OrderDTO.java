package artem.hotelproject.hotelservlet.DTO;


import artem.hotelproject.hotelservlet.model.Room;

import java.time.LocalDate;


public class OrderDTO {

    private Room room;
    private LocalDate date;
    private Long lengthOfStay;

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getLengthOfStay() {
        return lengthOfStay;
    }

    public void setLengthOfStay(Long lengthOfStay) {
        this.lengthOfStay = lengthOfStay;
    }
}
