package artem.hotelproject.hotelservlet.controller;

public final class Path {

    // pages
    public static final String PAGE_LOGIN = "/WEB-INF/jsp/login.jsp";
    public static final String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error.jsp";

    public static final String PAGE_ALL_ROOMS = "/WEB-INF/jsp/allRooms.jsp";
    public static final String PAGE_ALL_USERS_APPLICATIONS = "/WEB-INF/jsp/allUsersApplications.jsp";
    public static final String PAGE_APPLICATION = "/WEB-INF/jsp/application.jsp";
    public static final String PAGE_CLIENT_APPLICATIONS = "/WEB-INF/jsp/clientApplications.jsp";
    public static final String PAGE_ORDERS = "/WEB-INF/jsp/orders.jsp";
    public static final String PAGE_REGISTRATION = "/WEB-INF/jsp/registration.jsp";
    public static final String PAGE_ROOM_INFO = "/WEB-INF/jsp/roomInfo.jsp";
    public static final String PAGE_ROOM_ORDER = "/WEB-INF/jsp/roomOrder.jsp";

    // redirect
    public static final String REDIRECT_PAGE_ALL_ROOMS = "redirect:/all-rooms";
    public static final String REDIRECT_PAGE_LOGIN = "redirect:/login";
    public static final String REDIRECT_PAGE_ORDERS = "redirect:/orders";
    public static final String REDIRECT_PAGE_CLIENT_APPLICATIONS = "redirect:/client-applications";
    public static final String REDIRECT_PAGE_ALL_APPLICATIONS = "redirect:/all-applications";

}