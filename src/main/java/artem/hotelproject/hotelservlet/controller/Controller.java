package artem.hotelproject.hotelservlet.controller;

import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.service.OrderService;
import artem.hotelproject.hotelservlet.service.RoomService;
import artem.hotelproject.hotelservlet.web.Command;
import artem.hotelproject.hotelservlet.web.CommandContainer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class Controller extends HttpServlet {

    protected void doGet(HttpServletRequest request,
                         HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException, IOException {
        process(request, response);
    }

    private void process(HttpServletRequest request,
                         HttpServletResponse response) throws IOException, ServletException {

        String commandName = request.getRequestURI().split("/")[1];
        Command command = CommandContainer.get(commandName);
        String forward = Path.PAGE_ERROR_PAGE;

        try {
            forward = command.execute(request, response);
        } catch (Exception ex) {
            request.setAttribute("errorMessage", ex.getMessage());
        }
        if (forward.contains("redirect")) {

            String url = forward.replaceAll("redirect:", "");
            response.sendRedirect(url);
            return;
        }
        request.getRequestDispatcher(forward).forward(request, response);
    }
}