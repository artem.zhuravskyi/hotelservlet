package artem.hotelproject.hotelservlet.dao;

import artem.hotelproject.hotelservlet.model.Image;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.enums.Role;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;
import org.apache.log4j.Logger;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

import static artem.hotelproject.hotelservlet.dao.Messages.*;
import static artem.hotelproject.hotelservlet.dao.Messages.CANNOT_FIND_USER_BY_ID;

public class RoomDAO {


    private static final Logger logger = Logger.getLogger("OrderDAOLogger");
    private static RoomDAO instance;

    public static synchronized RoomDAO getInstance() {
        if (instance == null) {
            instance = new RoomDAO();
        }
        return instance;
    }


    public Room create(Room entity) {
        String sql = "INSERT INTO rooms (id, person_number, room_class, status, price) VALUES (DEFAULT, ?,?,?,?)";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, entity.getPersonNumber());
            statement.setString(2, entity.getRoomClass().name());
            statement.setString(3, entity.getStatus().name());
            statement.setLong(4, entity.getPrice());
            statement.execute();
        } catch (SQLException e) {
            logger.info(CANNOT_CREATE_USER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return entity;
    }

    public void update(Room entity) {
        String sql = "UPDATE rooms SET person_number = ?, room_class = ?, status = ?, price = ? WHERE id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, entity.getPersonNumber());
            statement.setString(2, entity.getRoomClass().name());
            statement.setString(3, entity.getStatus().name());
            statement.setLong(4, entity.getPrice());
            statement.setLong(5, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            logger.info(CANNOT_UPDATE_USER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
    }

    public Optional<Room> findById(Long id) {
        Room room = new Room();
        Image image = new Image();
        String find = "SELECT * FROM images as i join rooms as r on i.room_id = r.id WHERE r.id=?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(find)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            room.setPersonNumber(resultSet.getLong("person_number"));
            room.setRoomClass(RoomClass.valueOf(resultSet.getString("room_class").toUpperCase(Locale.ROOT)));
            room.setStatus(Room.Status.valueOf(resultSet.getString("status")));
            room.setPrice(resultSet.getLong("price"));
            room.setId(resultSet.getLong("room_id"));
            image.setBalcony(resultSet.getString("balcony"));
            image.setBedroom(resultSet.getString("bedroom"));
            image.setToilet(resultSet.getString("toilet"));
            room.setImages(image);
        } catch (SQLException e) {
            logger.info(CANNOT_FIND_USER_BY_ID.toString());
            room = null;
            System.out.println(e.getMessage());
        } finally {
            DataSource.closeConnection(con);
        }
        return Optional.ofNullable(room);
    }

    public List<Room> findAll() {
        List<Room> rooms = new ArrayList<>();
        Connection con = DataSource.getConnection();
        try (Statement statement = con.createStatement();
             ResultSet set = statement.executeQuery("SELECT * FROM images as i join rooms as r on i.room_id = r.id")) {
            while (set.next()) {
                Room room = new Room();
                Image image = new Image();
                room.setId((set.getLong("room_id")));
                room.setPersonNumber(set.getLong("person_number"));
                room.setRoomClass(RoomClass.valueOf(set.getString("room_class").toUpperCase(Locale.ROOT)));
                room.setStatus(Room.Status.valueOf(set.getString("status").toUpperCase(Locale.ROOT)));
                room.setPrice(set.getLong("price"));
                image.setBalcony(set.getString("balcony"));
                image.setBedroom(set.getString("bedroom"));
                image.setToilet(set.getString("toilet"));
                room.setImages(image);
                rooms.add(room);
            }
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return rooms;
    }

    public Optional<Integer> countIntersectionDateQuantity(Long roomId, LocalDate lastDate, LocalDate firstDate) {
        int count = 0;
        String find = "select count(o.id) from rooms as r join orders as o on r.id = o.room_id where r.id = ? " +
                "and (o.first_date <= ? and o.last_date >= ?) GROUP BY r.id";
        try (Connection con = DataSource.getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(find)) {
            preparedStatement.setLong(1, roomId);
            preparedStatement.setDate(2, Date.valueOf(lastDate.toString()));
            preparedStatement.setDate(3, Date.valueOf(firstDate.toString()));
            ResultSet set = preparedStatement.executeQuery();
            set.next();
            count = set.getInt(1);


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.of(count);
    }

    ;

}
