package artem.hotelproject.hotelservlet.dao;

import artem.hotelproject.hotelservlet.exception.UserException;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.Role;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static artem.hotelproject.hotelservlet.dao.Messages.*;

public class UserDAO {

    private static final Logger logger = Logger.getLogger("OrderDAOLogger");
    private static UserDAO instance;

    public static synchronized UserDAO getInstance() {
        if (instance == null) {
            instance = new UserDAO();
        }
        return instance;
    }

    public User create(User entity) throws UserException {
        String sql = "INSERT INTO users (id, email, first_name, last_name, password, role) VALUES (DEFAULT, ?,?,?,?,?)";

        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, entity.getEmail());
            statement.setString(2, entity.getFirstName());
            statement.setString(3, entity.getLastName());
            statement.setString(4, entity.getPassword());
            statement.setString(5, entity.getRole().name());
            statement.execute();

        } catch (SQLException e) {
            logger.info(CANNOT_CREATE_USER.toString());
            e.printStackTrace();
            throw new UserException("Cannot create");
        } finally {
            DataSource.closeConnection(con);
        }
        return entity;
    }

    public void update(User entity) {
        String sql = "UPDATE users SET first_name = ?, last_name = ?, password = ?, role = ? WHERE id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setString(3, entity.getPassword());
            statement.setString(4, entity.getRole().name());
            statement.setLong(5, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            logger.info(CANNOT_UPDATE_USER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
    }

    public Optional<User> findById(Long id) {
        User user = new User();
        String find = "SELECT * FROM users WHERE id=?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(find)) {
            statement.setLong(1, id);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            user.setPassword(resultSet.getString("password"));
            user.setEmail(resultSet.getString("email"));
            user.setLastName(resultSet.getString("last_name"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            user.setId(resultSet.getLong("id"));
        } catch (SQLException e) {
            logger.info(CANNOT_FIND_USER_BY_ID.toString());
            user = null;
            System.out.println(e.getMessage());
        } finally {
            DataSource.closeConnection(con);
        }
        return Optional.ofNullable(user);
    }

    public Optional<User> findUserByEmail(String email) {

        User user = new User();
        String sql = "SELECT * FROM users WHERE email=?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, email);
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();
            user.setPassword(resultSet.getString("password"));
            user.setEmail(resultSet.getString("email"));
            user.setLastName(resultSet.getString("last_name"));
            user.setFirstName(resultSet.getString("first_name"));
            user.setRole(Role.valueOf(resultSet.getString("role")));
            user.setId(resultSet.getLong("id"));
        } catch (SQLException e) {
            logger.info(CANNOT_FIND_USER_BY_ID.toString());
            user = null;
            System.out.println(e.getMessage());
        } finally {
            DataSource.closeConnection(con);
        }

        return Optional.ofNullable(user);
    }

    ;


}
