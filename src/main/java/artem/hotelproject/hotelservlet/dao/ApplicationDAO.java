package artem.hotelproject.hotelservlet.dao;

import artem.hotelproject.hotelservlet.model.Application;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.logging.Logger;

public class ApplicationDAO {
    private static final Logger logger = Logger.getLogger("OrderDAOLogger");
    private static ApplicationDAO instance;
    private static final String sql_insert_into_applications_room =
            "INSERT INTO applications_room (application_id, room_id) " +
                    "VALUES (?,?)";
    private static final String sql_application_exist =
            "SELECT * FROM applications a WHERE a.first_date = ? and a.last_date = ? and" +
                    " a.room_class = ? and a.client_id=?";

    public static synchronized ApplicationDAO getInstance() {
        if (instance == null) {
            instance = new ApplicationDAO();
        }

        return instance;
    }

    public void createEntityToManyToManyTable(List<Room> rooms, Long id, Connection con) throws SQLException {

        rooms.forEach(room -> {
            try {

                PreparedStatement statement = con.prepareStatement(sql_insert_into_applications_room);
                statement.setLong(1, id);
                statement.setLong(2, room.getId());
                statement.execute();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public Application create(Application entity) {
        String sqlToInsert = "INSERT INTO applications (id, first_date, last_date, room_class, client_id)" +
                " VALUES (DEFAULT, ?, ?, ?, ?)";
        String sqlSelectApplicationId = "SELECT * FROM applications ORDER BY id DESC LIMIT 1";


        try (Connection con = DataSource.getConnection();
             PreparedStatement statementToInsert = con.prepareStatement(sqlToInsert);
             Statement statement = con.createStatement()) {
            con.setAutoCommit(false);
            try {
                statementToInsert.setDate(1, Date.valueOf(entity.getFirstDate()));
                statementToInsert.setDate(2, Date.valueOf(entity.getLastDate()));
                statementToInsert.setString(3, entity.getRoomClass().name());
                statementToInsert.setLong(4, entity.getClient().getId());
                statementToInsert.execute();

                ResultSet set = statement.executeQuery(sqlSelectApplicationId);
                set.next();
                createEntityToManyToManyTable(entity.getRoom(), set.getLong("id"), con);
            } catch (SQLException e) {
                con.rollback();
                e.printStackTrace();
            }
            con.commit();
        } catch (SQLException e) {

            logger.info(Messages.CANNOT_CREATE_ORDER.toString());
            e.printStackTrace();
        }
        return entity;
    }

    public List<Application> findAllByClient(User user) {
        List<Application> applications = new ArrayList<>();
        String sql = "SELECT * FROM applications as o JOIN users as u ON u.id = o.client_id WHERE u.id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, user.getId());
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                Application application = new Application();
                application.setId(set.getLong("id"));
                application.setFirstDate(LocalDate.parse(set.getString("first_date")));
                application.setLastDate(LocalDate.parse(set.getString("last_date")));
                application.setRoomClass(RoomClass.valueOf(set.getString("room_class").toUpperCase(Locale.ROOT)));
                applications.add(application);
            }
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return applications;
    }

    public void deleteById(Long id) {
        String sqlDeleteApp = "DELETE FROM applications WHERE id = ?";
        try (Connection con = DataSource.getConnection();
             PreparedStatement statement = con.prepareStatement(sqlDeleteApp);
        ) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_DELETE_PRODUCT.toString());
            e.printStackTrace();
//            throw new UserException();
        }
    }

    public List<Application> findAll() {
        List<Application> applications = new ArrayList<>();

        try (Connection con = DataSource.getConnection();
             Statement statement = con.createStatement();
             ResultSet set = statement.executeQuery("SELECT * FROM applications");
             PreparedStatement preparedStatement = con.prepareStatement("select room_id from applications_room" +
                     " where application_id = ?")) {
            con.setAutoCommit(false);
            try {
                while (set.next()) {
                    UserDAO userDao = UserDAO.getInstance();
                    RoomDAO roomDAO = RoomDAO.getInstance();
                    List<Room> rooms = new ArrayList<>();
                    Application application = new Application();
                    application.setId((set.getLong("id")));
                    application.setFirstDate(LocalDate.parse(set.getString("first_date")));
                    application.setClient(userDao.findById(set.getLong("client_id")).get());
                    application.setLastDate(LocalDate.parse(set.getString("last_date")));
                    application.setRoomClass(RoomClass.valueOf(set.getString("room_class").toUpperCase(Locale.ROOT)));
                    preparedStatement.setLong(1, set.getLong("id"));
                    preparedStatement.execute();
                    ResultSet rs = preparedStatement.getResultSet();
                    while (rs.next()) {
                        rooms.add(roomDAO.findById(rs.getLong("room_id")).get());
                    }
                    application.setRoom(rooms);
                    applications.add(application);
                }
            } catch (SQLException e) {
                con.rollback();
                e.printStackTrace();
            }
            con.commit();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        }
        return applications;
    }

    public Optional<Application> findById(Long id) {
        Application application = new Application();
        UserDAO userDAO = UserDAO.getInstance();
        RoomDAO roomDAO = RoomDAO.getInstance();
        try (Connection con = DataSource.getConnection();
             PreparedStatement preparedStatement = con.prepareStatement("select * from applications a " +
                     "join applications_room ar on a.id = ar.application_id where a.id = ?")) {
            con.setAutoCommit(false);

            try {
                List<Room> rooms = new ArrayList<>();

                preparedStatement.setLong(1, id);
                preparedStatement.execute();
                ResultSet set = preparedStatement.getResultSet();
                set.next();
                application.setId((set.getLong("id")));
                application.setFirstDate(LocalDate.parse(set.getString("first_date")));
                application.setClient(userDAO.findById(set.getLong("client_id")).get());
                application.setLastDate(LocalDate.parse(set.getString("last_date")));
                application.setRoomClass(RoomClass.valueOf(set.getString("room_class").toUpperCase(Locale.ROOT)));
                rooms.add(roomDAO.findById(set.getLong("room_id")).get());
                while (set.next()) {
                    rooms.add(roomDAO.findById(set.getLong("room_id")).get());
                }
                application.setRoom(rooms);
            } catch (SQLException e) {
                e.printStackTrace();
                con.rollback();
            }
            con.commit();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        }
        return Optional.of(application);
    }

    public boolean isApplicationExist(Application application) {

        try (Connection con = DataSource.getConnection();
             PreparedStatement preparedStatement = con.prepareStatement(sql_application_exist)) {
            preparedStatement.setDate(1, Date.valueOf(application.getFirstDate()));
            preparedStatement.setDate(2, Date.valueOf(application.getLastDate()));
            preparedStatement.setString(3, application.getRoomClass().name());
            preparedStatement.setLong(4, application.getClient().getId());
            preparedStatement.execute();
            ResultSet set = preparedStatement.executeQuery();
            set.next();
            int count = set.getInt(1);
            if (count > 0) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }
}
