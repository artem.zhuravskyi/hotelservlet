package artem.hotelproject.hotelservlet.dao;

import artem.hotelproject.hotelservlet.model.Order;
import artem.hotelproject.hotelservlet.model.Room;
import artem.hotelproject.hotelservlet.model.User;
import artem.hotelproject.hotelservlet.model.enums.RoomClass;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

import static artem.hotelproject.hotelservlet.dao.Messages.CANNOT_UPDATE_USER;

public class OrderDAO {

    private static final Logger logger = Logger.getLogger("OrderDAOLogger");
    private static OrderDAO instance;

    public static synchronized OrderDAO getInstance() {
        if (instance == null) {
            instance = new OrderDAO();
        }

        return instance;
    }

    public Order create(Order entity) {
        String sqlToInsert = "INSERT INTO orders (id, first_date, last_date, creation_date, price, status, client_id, room_id)" +
                " VALUES (DEFAULT,?,?,?,?,?,?,?)";
        Connection con =  DataSource.getConnection();
        try (PreparedStatement statementToInsert = con.prepareStatement(sqlToInsert)) {
            statementToInsert.setDate(1, Date.valueOf(entity.getFirstDate()));
            statementToInsert.setDate(2, Date.valueOf(entity.getLastDate()));
            statementToInsert.setDate(3, Date.valueOf(LocalDate.now()));
            statementToInsert.setLong(4, entity.getPrice());
            statementToInsert.setString(5, entity.getStatus().name());
            statementToInsert.setLong(6, entity.getClient().getId());
            statementToInsert.setLong(7, entity.getRoom().getId());
            statementToInsert.execute();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_CREATE_ORDER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return entity;
    }


    public void update(Order entity) {
        String sql = "UPDATE orders SET first_date = ?, last_date = ?, creation_date = ?, price = ?, status = ? WHERE id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setDate(1, Date.valueOf(entity.getFirstDate()));
            statement.setDate(2, Date.valueOf(entity.getLastDate()));
            statement.setDate(3, Date.valueOf(entity.getCreationDate()));
            statement.setLong(4, entity.getPrice());
            statement.setString(5, entity.getStatus().toString());
            statement.setLong(6, entity.getId());
            statement.execute();
        } catch (SQLException e) {
            logger.info(CANNOT_UPDATE_USER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
    }


    public Optional<Order> findById(Long id) {
        Order order = new Order();
        Connection con = DataSource.getConnection();
        String sql = "SELECT * FROM orders WHERE id = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            set.next();
            order.setId(id);
            order.setFirstDate(LocalDate.parse(set.getString("first_date")));
            order.setLastDate(LocalDate.parse(set.getString("last_date")));
            order.setCreationDate(LocalDate.parse(set.getString("creation_date")));
            order.setPrice(set.getLong("price"));
            order.setStatus(Order.Status.valueOf(set.getString("status")));
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ORDER_BY_ID.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return Optional.of(order);
    }

    public List<Order> findAllByClient(User client) {
        List<Order> orders = new ArrayList<>();
        String sql = "SELECT * FROM orders as o JOIN users as u ON u.id = o.client_id WHERE u.id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, client.getId());
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                Order order = new Order();
                RoomDAO roomDAO = RoomDAO.getInstance();
                UserDAO userDAO = UserDAO.getInstance();
                order.setId(set.getLong("id"));
                order.setFirstDate(LocalDate.parse(set.getString("first_date")));
                order.setLastDate(LocalDate.parse(set.getString("last_date")));
                order.setCreationDate(LocalDate.parse(set.getString("creation_date")));
                order.setPrice(set.getLong("price"));

                order.setClient(userDAO.findById(set.getLong("client_id")).get());
                order.setRoom(roomDAO.findById(set.getLong("room_id")).get());
                order.setStatus(Order.Status.valueOf(set.getString("status")));
                orders.add(order);
            }
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return orders;
    }

    public Order findByClientAndId(User client, Long id) {
        Order order = new Order();
        Connection con = DataSource.getConnection();
        String sql = "SELECT * FROM orders as o JOIN users as u ON u.id = o.client_id WHERE u.id = ? AND o.id = ?";
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, client.getId());
            statement.setLong(2, id);
            statement.execute();
            ResultSet set = statement.getResultSet();
            set.next();
            order.setFirstDate(LocalDate.parse(set.getString("first_date")));
            order.setLastDate(LocalDate.parse(set.getString("last_date")));
            order.setCreationDate(LocalDate.parse(set.getString("creation_date")));
            order.setPrice(set.getLong("price"));
            order.setStatus(Order.Status.valueOf(set.getString("status")));
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ORDER_BY_ID.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return order;
    }

    public List<Order> findAllByRoomAndLastDateAfter(Room room, LocalDate localDate) {
        List<Order> orders = new ArrayList<>();
        Connection con = DataSource.getConnection();
        String sql = "SELECT * FROM orders as o JOIN rooms as r ON r.id = o.room_id WHERE r.id = ? and o.last_date > ?";

        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, room.getId());
            statement.setDate(2, Date.valueOf(localDate));
            statement.execute();
            ResultSet set = statement.getResultSet();
            while (set.next()) {
                Order order = new Order();
                order.setId(set.getLong("id"));
                order.setFirstDate(LocalDate.parse(set.getString("first_date")));
                order.setLastDate(LocalDate.parse(set.getString("last_date")));
                order.setCreationDate(LocalDate.parse(set.getString("creation_date")));
                order.setPrice(set.getLong("price"));
                order.setStatus(Order.Status.valueOf(set.getString("status")));
                orders.add(order);
            }
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_FIND_ALL_USERS.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }

        return orders;
    }

    public void deleteById(Long id) {
        String sql = "DELETE FROM orders WHERE id = ?";
        Connection con = DataSource.getConnection();
        try (PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setLong(1, id);
            statement.execute();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_DELETE_PRODUCT.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
    }

}
