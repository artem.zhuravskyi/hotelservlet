package artem.hotelproject.hotelservlet.dao;

import artem.hotelproject.hotelservlet.model.Invoice;
import artem.hotelproject.hotelservlet.model.Order;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Logger;

import static artem.hotelproject.hotelservlet.dao.Messages.CANNOT_UPDATE_USER;

public class InvoiceDAO {


    private static final Logger logger = Logger.getLogger("OrderDAOLogger");
    private static InvoiceDAO instance;

    public static synchronized InvoiceDAO getInstance() {
        if (instance == null) {
            instance = new InvoiceDAO();
        }

        return instance;
    }

    public Invoice create(Invoice entity) {
        String sqlToInsert = "INSERT INTO invoices (id)" +
                " VALUES (DEFAULT)";
        Connection con =  DataSource.getConnection();
        try (PreparedStatement statementToInsert = con.prepareStatement(sqlToInsert)) {
            statementToInsert.execute();
        } catch (SQLException e) {
            logger.info(Messages.CANNOT_CREATE_ORDER.toString());
            e.printStackTrace();
        } finally {
            DataSource.closeConnection(con);
        }
        return entity;
    }

}
