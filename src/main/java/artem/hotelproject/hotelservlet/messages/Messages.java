package artem.hotelproject.hotelservlet.messages;

public class Messages {

    public final static String USER_NOT_FOUND_MSG = "User with email %s not found";
    public final static String USER_ALREADY_EXISTS_MSG = "User with email %s already exists";
    public final static String DATE_IS_TAKEN = "These dates are taken";
}
