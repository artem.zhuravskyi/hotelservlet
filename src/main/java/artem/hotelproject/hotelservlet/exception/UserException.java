package artem.hotelproject.hotelservlet.exception;

public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }
}